﻿using Slugify;
using System.Collections.Generic;
using System.Linq;
using TiendaMusica.Data.Repositorio;
using TiendaMusica.Logica.Comun;
using TiendaMusica.Logica.ViewModels;
using System;

namespace TiendaMusica.Logica
{
    public class TiendaConsultas
    {
        private readonly ITiendaMusicaRepository db;
        public TiendaConsultas(ITiendaMusicaRepository repositorio)
        {
            db = repositorio;
        }
        public IEnumerable<AlbumsPorArtistaViewModel> Albums(string slug)
        {
            var x = db.Albums.GetAll()
                    .Where(a => a.Artist.Slug == slug).ToList();

            return db.Albums.GetAll()
                    .Where(a => a.Artist.Slug == slug)
                    .Select(o => new AlbumsPorArtistaViewModel
                    {
                        Album = o.Title,
                        Artista = o.Artist.Name,
                        AlbumSlug = o.Slug,
                        ArtistaSlug = o.Artist.Slug,
                        ImagenPath = (o.ImagePath == null) ? "no-disponible.png" : o.ImagePath
                    }).ToList();

        }

        public IEnumerable<ArtistasViewModel> Artistas()
        {
            return db.Artistas.GetAll()
                .Select(o => new ArtistasViewModel
                {
                    Artista = o.Name,
                    Slug = o.Slug
                }).ToList();
        }

        public AlbumEditarViewModel EditarAlbum(string nombre)
        {
            
            return db.Albums.GetAll()
                .Where(a => a.Slug == nombre)
                .Select(o => new AlbumEditarViewModel
                {
                    Titulo = o.Title,
                    Slug = o.Slug,
                    Imagen = (o.ImagePath == null) ? "no-disponible.png" : o.ImagePath,
                    ArtistaSlug = o.Artist.Slug
                }).First();

        }

        public void ActualizarAlbum(string slug, AlbumEditarViewModel album)
        {

            var x = db.Albums.GetAll().Where(a => a.Slug == slug).First();
            x.ImagePath = album.Imagen;
            x.Title = album.Titulo;
            db.Commit();

        }


        public IEnumerable<CancionesPorAlbumViewModel> Canciones(string slug)
        {

            return db.Canciones.GetAll()
                    .Where(a => a.Album.Slug == slug)
                    .Select(o => new CancionesPorAlbumViewModel
                    {
                        Cancion = o.Name,
                        CancionSlug = o.Slug,
                        Tamano = o.Bytes,
                        AlbumSlug = o.Album.Slug,
                        ArtistaSlug = o.Album.Artist.Slug,
                        FilePath = o.FilePath
                    }).ToList();


        }

        public CancionEditarViewModel EditarCancion(string slug)
        {
            return db.Canciones.GetAll().Where(x => x.Slug == slug).Select(o => new CancionEditarViewModel
            {
                Nombre = o.Name,
                CancionSlug = o.Slug,
                AlbumSlug = o.Album.Slug,
                ArtistaSlug = o.Album.Artist.Slug,
                FilePath = o.FilePath,
                Tamano = o.Bytes
            }).First();

        }

        public void ActualizarCancion(string slug, CancionEditarViewModel cancion)
        {

            var x = db.Canciones.GetAll().Where(a => a.Slug == slug).First();
            x.FilePath = cancion.FilePath;
            x.Bytes = cancion.Tamano;
            x.Name = cancion.Nombre;
            db.Commit();

        }

    }
}