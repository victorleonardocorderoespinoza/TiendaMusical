﻿using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaMusica.Logica
{
    public class ReporteSimple
    {
        public static string Reporte()
        {
            HSSFWorkbook libro = new HSSFWorkbook();

            var hoja = libro.CreateSheet("Ejemplo reporte");
            var fila = hoja.CreateRow(1);
            var celda = fila.CreateCell(1);

            celda.SetCellValue("Ejemplo");

            for (int i = 3; i < 6; i++)
            {
                var filan = hoja.CreateRow(i);
                var celda1 = fila.CreateCell(1);
                celda1.SetCellValue("Fila " + i);
                celda1.CellStyle.WrapText = true;

                var celda2 = fila.CreateCell(2);
                celda2.SetCellValue(DateTime.Now.AddDays(i).ToLongDateString());

            }
            var nombreArchivo = NombreArchivo();
            var filestream = new FileStream(nombreArchivo, FileMode.CreateNew);
            libro.Write(filestream);
            filestream.Flush();
            filestream.Close();

            
            return nombreArchivo;

        }

        private static string NombreArchivo()
        {
            var ruta = Path.GetTempFileName();
            File.Delete(ruta);
            return Path.ChangeExtension(ruta, ".xls");
             
        }
         

        public static Stream Excel()
        {
            HSSFWorkbook libro = new HSSFWorkbook();

            var hoja = libro.CreateSheet("Ejemplo reporte");
            var fila = hoja.CreateRow(1);
            var celda = fila.CreateCell(1);

            celda.SetCellValue("Ejemplo");

            for (int i = 3; i < 6; i++)
            {
                var filan = hoja.CreateRow(i);
                var celda1 = filan.CreateCell(1);
                celda1.SetCellValue("Fila " + i);
                celda1.CellStyle.WrapText = true;

                var celda2 = filan.CreateCell(2);
                celda2.SetCellValue(DateTime.Now.AddDays(i).ToLongDateString());

            } 

            var filestream = new MemoryStream();
            libro.Write(filestream);
            filestream.Position = 0;

            return filestream;

        }
 
    }
}
