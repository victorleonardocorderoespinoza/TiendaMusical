﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TiendaMusica.Logica.ViewModels
{
    public class CancionEditarViewModel
    {
        [Required(ErrorMessage = "El Nombre es obligatorio")]
        public string Nombre { get; set; }
        public string CancionSlug { get; set; }
        public string AlbumSlug { get; set; }
        public string ArtistaSlug { get; set; }
        public string FilePath { get; set; } 
        [DisplayName("Tamaño")]
        public int? Tamano { get; set; }

    }
}
