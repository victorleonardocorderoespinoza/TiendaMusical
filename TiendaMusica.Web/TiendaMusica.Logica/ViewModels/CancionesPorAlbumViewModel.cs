﻿using System.ComponentModel;

namespace TiendaMusica.Logica.ViewModels
{
    public class CancionesPorAlbumViewModel
    {
        [DisplayName("Duración")]
        public string Cancion { get; set; }
        [DisplayName("Tamaño")]
        public int? Tamano { get; set; }
        public string CancionSlug { get; set; }
        public string AlbumSlug { get; set; }
        public string ArtistaSlug { get; set; }
        public string FilePath { get; set; }

    }
}
