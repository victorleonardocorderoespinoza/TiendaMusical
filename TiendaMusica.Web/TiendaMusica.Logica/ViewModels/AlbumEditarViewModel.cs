﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TiendaMusica.Logica.ViewModels
{
    public class AlbumEditarViewModel
    {
        [Required(ErrorMessage = "El título es obligatorio")]
        [DisplayName("Título")]
        public string Titulo { get; set; }
        public string Slug { get; set; }
        public string Imagen { get; set; }
        public string ArtistaSlug { get; set; }
    }
}
