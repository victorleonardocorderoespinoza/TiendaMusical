﻿namespace TiendaMusica.Logica.ViewModels
{
    public class AlbumsPorArtistaViewModel
    {
        public string Artista { get; set; }
        public string Album { get; set; }
        public string AlbumSlug { get; set; }
        public string ArtistaSlug { get; set; }
        public string ImagenPath { get; set; }
    }
}
