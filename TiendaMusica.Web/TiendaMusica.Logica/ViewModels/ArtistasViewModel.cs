﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TiendaMusica.Logica.ViewModels
{
    public class ArtistasViewModel
    {
        public string Artista { get; set; }
        public string Slug { get; set; }
    }
}
