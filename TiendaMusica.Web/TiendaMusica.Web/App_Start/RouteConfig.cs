﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TiendaMusica.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "ArtistList",
                url: "artists",
                defaults: new { controller = "Artistas", action = "List" }
                );


            routes.MapRoute(
                name: "AlbumList",
                url: "{artista}/{action}",
                defaults: new { controller = "Artistas"}
                );

            routes.MapRoute(
               name: "TrackList",
               url: "{artista}/{album}/{action}",
               defaults: new { controller = "Albums" }
               );


            routes.MapRoute(
                name: "AlbumEdit",
                url: "admin/{artista}/{album}/{action}",
                defaults: new { controller = "Albums" }
                );

            routes.MapRoute(
              name: "TrackEdit",
              url: "admin/{artista}/{album}/{cancion}/{action}",
              defaults: new { controller = "Canciones" }
              );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
