﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using TiendaMusica.Infraestructura;
using TiendaMusica.Logica;
using TiendaMusica.Logica.ViewModels;
using TiendaMusica.Web.Utilidades;

namespace TiendaMusica.Web.Controllers
{
    public class AlbumsController : Controller
    {
        private readonly TiendaConsultas tienda;
        public AlbumsController()
        {
            tienda = ConstructorServicios.TiendaConsultas();
        }
        public ActionResult Edit(string artista, string album)
        {
            AlbumEditarViewModel model = tienda.EditarAlbum(album);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(string artista, string album, HttpPostedFileBase archivo, AlbumEditarViewModel model)
        {
            if (archivo != null)
            {
                if ((archivo.ContentType).Contains("image"))
                {
                    string nombreArchivo = archivo.FileName.ObtenerMD5() + Path.GetExtension(archivo.FileName);
                    GrabarImagen(archivo, nombreArchivo);
                    model.Imagen = nombreArchivo;
                }
            }

            if (ModelState.IsValid)
            {
                tienda.ActualizarAlbum(album, model);
                return Redirect(string.Format("/{0}/albums", artista));
            }
            else
            {
                return View(model);
            }

        } 
        private void GrabarImagen(HttpPostedFileBase archivo, string nombreArchivo)
        {
            MemoryStream ms = new MemoryStream();
            archivo.InputStream.CopyTo(ms);
            Imagen imagen = new Imagen(ms, archivo.FileName, archivo.ContentType, Server.MapPath("~/Albums"));
            imagen.Grabar(nombreArchivo, Server.MapPath("~/Albums/Thumbnails"));

        }

        public ActionResult Tracks(string artista, string album) {

            IEnumerable<CancionesPorAlbumViewModel> canciones =  tienda.Canciones(album);
            return View(canciones);
        }
    }
}