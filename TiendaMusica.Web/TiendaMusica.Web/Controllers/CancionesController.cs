﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TiendaMusica.Infraestructura;
using TiendaMusica.Logica;
using TiendaMusica.Logica.ViewModels;
using TiendaMusica.Web.Utilidades;

namespace TiendaMusica.Web.Controllers
{
    public class CancionesController : Controller
    {

        private readonly TiendaConsultas tienda;
        public CancionesController()
        {
            tienda = ConstructorServicios.TiendaConsultas();
        }

        public ActionResult Edit(string artista, string album, string cancion)
        {
            CancionEditarViewModel model = tienda.EditarCancion(cancion);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(string artista, string album, string cancion, HttpPostedFileBase archivo, CancionEditarViewModel model)
        {

            if (archivo != null)
            {
                if ((archivo.ContentType).Contains("audio"))
                {
                    string nombreArchivo = archivo.FileName.ObtenerMD5() + Path.GetExtension(archivo.FileName);
                    GrabarAudio(archivo, nombreArchivo);
                    model.FilePath = nombreArchivo;
                }
            }

            if (ModelState.IsValid)
            {
                tienda.ActualizarCancion(cancion, model);
                return Redirect(string.Format("/{0}/{1}/tracks", artista, album));
            }
            else
            {
                return View(model);
            }

        }

        private void GrabarAudio(HttpPostedFileBase archivo, string nombreArchivo)
        {
            MemoryStream ms = new MemoryStream();
            archivo.InputStream.CopyTo(ms);
            Audio audio = new Audio(ms, archivo.FileName, archivo.ContentType, Server.MapPath("~/Tracks"));
            audio.Grabar(nombreArchivo);
        }


    }
}
