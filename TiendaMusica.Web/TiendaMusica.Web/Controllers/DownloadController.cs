﻿using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TiendaMusica.Logica;

namespace TiendaMusica.Web.Controllers
{
    public class DownloadController : Controller
    {
        // GET: Download
        public ActionResult Reporte(string id)
        {

            string[] fileEntries = Directory.GetFiles(Server.MapPath("~/Downloads"), "*." + id);
            foreach (var item in fileEntries)
            {
                FilePathResult download = new FilePathResult(item, MimeMapping.GetMimeMapping(fileEntries[0]));
                download.FileDownloadName = Path.GetFileName(item);
                return download;
            }
              
            return Redirect("~/Error/Error404");

        }

        public ActionResult Excel()
        {

            FileStreamResult download = new FileStreamResult(ReporteSimple.Excel(), "application/vnd.ms-excel");
            download.FileDownloadName = "Excel.xls";

            return download;

        }
    }
}