﻿using System;
using System.Drawing;
using System.IO;

namespace TiendaMusica.Web.Utilidades
{
    internal class Imagen
    {

        public Imagen(Stream imagen, string archivo, string contentType, string rutaServidor)
        {

            this.Bytes = imagen;
            this.NombreArchivo = archivo;
            this.TipoContenido = contentType;
            this.Ruta = rutaServidor;

        }

        public Stream Bytes { get; private set; }
        public string NombreArchivo { get; private set; }
        public string Ruta { get; private set; }
        public string TipoContenido { get; private set; }

        public void Grabar(string nombre, string thumbnailPath)
        {
            GuardarArchivoOriginal(nombre);
            GuardarThumbnails(nombre, thumbnailPath);
        }

        private void GuardarThumbnails(string nombre, string thumbnailPath)
        {
           
            if (File.Exists(Path.Combine(thumbnailPath, nombre)))
            {
                File.Delete(Path.Combine(thumbnailPath, nombre));
            }

            using (var image = Image.FromFile(Path.Combine(Ruta, nombre)))
            {

                var escala = Math.Min((float)200 / image.Width, (float)200 / image.Height);
                var escalaWidth = (int)(image.Width * escala);
                var escalaHeight = (int)(image.Height * escala);

                using (var thumbnail = image.GetThumbnailImage(escalaWidth, escalaHeight, null, IntPtr.Zero))
                {
                    thumbnail.Save(Path.Combine(thumbnailPath, nombre));
                }
            }


        }


        private void GuardarArchivoOriginal(string nombre)
        {
            if (File.Exists(Path.Combine(Ruta, nombre)))
            {
                File.Delete(Path.Combine(Ruta, nombre));
            }
            FileStream fs = new FileStream(Path.Combine(Ruta, nombre), FileMode.Create);
            Bytes.Position = 0;
            Bytes.CopyTo(fs);
            fs.Position = 0;
            fs.Flush();
            fs.Close();

        }
    }
}
