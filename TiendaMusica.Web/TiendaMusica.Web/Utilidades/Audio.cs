﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace TiendaMusica.Web.Utilidades
{
    internal class Audio
    {

        public Stream Bytes { get; private set; }
        public string NombreArchivo { get; private set; }
        public string Ruta { get; private set; }
        public string TipoContenido { get; private set; }

        public Audio(Stream audio, string archivo, string contentType, string rutaServidor)
        {
            this.Bytes = audio;
            this.NombreArchivo = archivo;
            this.TipoContenido = contentType;
            this.Ruta = rutaServidor;

        } 

        public void Grabar(string nombre)
        {
            GuardarArchivoOriginal(nombre);
        }

        private void GuardarArchivoOriginal(string nombre)
        {
            if (File.Exists(Path.Combine(Ruta, nombre)))
            {
                File.Delete(Path.Combine(Ruta, nombre));
            }
            FileStream fs = new FileStream(Path.Combine(Ruta, nombre), FileMode.Create);
            Bytes.Position = 0;
            Bytes.CopyTo(fs);
            fs.Position = 0;
            fs.Flush();
            fs.Close();
        }

    }
}