﻿using System.Security.Cryptography;
using System.Text;

namespace System
{
    public static class Extensiones
    {
        public static string ObtenerMD5(this string cadena) {

            MD5 md5 = MD5.Create();
            byte[] hasData = md5.ComputeHash(Encoding.Default.GetBytes(cadena));
            StringBuilder value = new StringBuilder();
            for (int i = 0; i < hasData.Length; i++)
            {
                value.Append(hasData[i]);
            }
            return value.ToString();
        }
    }
}