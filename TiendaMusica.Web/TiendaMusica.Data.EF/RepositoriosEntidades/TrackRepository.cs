﻿using TiendaMusica.Data.Repositorio;
using TiendaMusica.Dominio;

namespace TiendaMusica.Data.EF.RepositoriosEntidades
{
    class TrackRepository : EFGenericRepository<EFTiendaMusicaRepository, Track>
    {
        public TrackRepository(EFTiendaMusicaRepository contexto)
            : base(contexto)
        {

        }
    }
}
