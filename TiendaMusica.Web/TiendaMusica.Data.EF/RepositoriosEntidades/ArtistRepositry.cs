﻿using TiendaMusica.Data.Repositorio;
using TiendaMusica.Dominio;

namespace TiendaMusica.Data.EF.RepositoriosEntidades
{
    class ArtistRepositry : EFGenericRepository<EFTiendaMusicaRepository, Artist>
    {
        public ArtistRepositry(EFTiendaMusicaRepository contexto)
            : base(contexto)
        {

        }
    }
}
